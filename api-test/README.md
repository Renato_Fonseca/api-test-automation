# Usando via docker run

Passo 1 -> docker build -t test-api/mocha .

Passo 2 -> docker run test-api/mocha

# Usando via docker-compose

Passo único -> docker-compose up

# Exception (caso esteja usando Windows ou sem Docker Desktop na maquina)

Passo unico -> npm test (na pasta api-test)



