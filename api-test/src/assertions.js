const {should} = require('chai')
const {expect} = require('chai')

module.exports = {
    should,
    expect
}
