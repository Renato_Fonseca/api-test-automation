const {chaiRequest} = require('../src/request')
const {BASE_URL} = require('../src/config')
const {expect} = require('../src/assertions')

describe('Cenário 1', () => {
  
  describe('Posts', () => {
        it('Testando GET todos os Posts', (done) => {
          chaiRequest.request(BASE_URL)
              .get(`/posts`)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    console.log(res.body)
                  done();
                });
        });
    });
  })
  describe('Cenário 2', () => {
  
    describe('Posts', () => {
          it('Testando PUT os posts', (done) => {
            const result = {
              postId: 54,
              id: 123,
              name: 'eu vou testar',
              email: 'teste@teste',
              body: 'Estou testando um put'
            }
            chaiRequest.request(BASE_URL)
            .put(`/posts/1`)
                .send(result)
                  .end((err, res) => {
                      expect(res.status).to.equal(200);
                      expect(res.body).find(element => element.postId === result.postId)
                     .should.equal(true)
                      console.log(res.body)
                    done();
                  });
          });
      });
    })

    describe('Cenário 3', () => {
  
      describe('Posts', () => {
            it('Testando GET todos os comments', (done) => {
              chaiRequest.request(BASE_URL)
                  .get(`/comments`)
                    .end((err, res) => {
                        expect(res.status).to.equal(200);
                        expect(res.type).to.contain.string('application/json');
                        console.log(res.body)
                        done();
                      });
            });
            
        });
      })

      describe('Cenário 4', () => {
  
        describe('Posts', () => {
              it('Testando Delete os posts ', (done) => {
                chaiRequest.request(BASE_URL)
                    .delete(`/posts/1`)
                      .end((err, res) => {
                          expect(res.status).to.equal(200);
                          console.log(res.body)
                        done();
                      });
              });
          });
        })
        
  